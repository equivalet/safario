const ROOT_API = `${process.env.ROOT_API}`
const APP = `safarios`
const ENDPOINT = `${ROOT_API}/${APP}/`

export default {
  data () {
    return {
      safarios: [],
      safario: undefined,
      paginationSafario: {
        next: undefined,
        previous: undefined,
        count: 0
      },
      querySafario: {
        page: 1
      }
    }
  },
  methods: {
    listSafario () {
      return this.axios.get(ENDPOINT, {params: this.querySafario})
        .then((response) => {
          this.safarios = response.data.results
          this.paginationSafario.next = response.data.next
          this.paginationSafario.previous = response.data.previous
          this.paginationSafario.count = response.data.count
        })
    },
    retrieveSafario (id) {
      return this.axios.get(`${ENDPOINT}${id}/`)
        .then((response) => {
          this.safario = response.data
        })
    },
    updateSafario (id, payload) {
      return this.axios.patch(`${ENDPOINT}${id}/`, payload)
        .then((response) => {
          this.safario = response.data
        })
    },
    createSafario (payload) {
      return this.axios.post(ENDPOINT, payload)
        .then((response) => {
          this.safario = response.data
        })
    },
    destroySafario (id) {
      return this.axios.delete(`${ENDPOINT}${id}/`)
        .then((response) => {
          this.safario = undefined
        })
    },
    exportSafario (prefix) {
      const filename = `${prefix}_${new Date()}_.csv`
      return this.axios({
        url: `${ENDPOINT}export/`,
        method: 'GET',
        responseType: 'blob',
        params: this.querySafario
      })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response.data]))
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', filename)
          document.body.appendChild(link)
          link.click()
        })
    }
  }
}
