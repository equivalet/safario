import Vue from 'vue'
import Router from 'vue-router'
import SafarioExampleManager from '@/Application/Safarios/Apps/SafarioExampleManager'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'SafarioExampleManager',
      component: SafarioExampleManager,
      meta: {
        requiresAuth: false
        // loggedCheck: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.loggedCheck)) {
    if (localStorage.getItem('token') !== null) {
      next({
        name: 'DashboardMainManager'
      })
    } else {
      next()
    }
  }

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('token') === null) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
