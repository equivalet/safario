export default {
  methods: {
    confirmDestroy (subject) {
      const config = {
        title: 'Please Confirm',
        size: 'sm',
        buttonSize: 'sm',
        okVariant: 'danger',
        okTitle: 'YES',
        cancelTitle: 'NO',
        footerClass: 'p-2',
        hideHeaderClose: false,
        centered: true
      }
      const message = `Please confirm that you want to delete ${subject}.`
      return this.$bvModal.msgBoxConfirm(message, config)
    },
    toastWarning () {
      this.$bvToast.toast(`You cannot take this action because there are some rules regarding the policy regarding it!`, {
        title: `Warning!`,
        toaster: 'b-toaster-top-center',
        solid: true,
        variant: 'warning',
        appendToast: true
      })
    }
  }
}
